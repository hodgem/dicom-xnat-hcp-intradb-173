/*
 * dicom-xnat-mx: org.nrg.dcm.xnat.CTScanAttributes
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.xnat;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import org.dcm4che2.data.Tag;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.TransformingExtAttrDef;
import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;
import org.nrg.dcm.xnat.XnatAttrDef;
import org.nrg.session.BeanBuilder;
import org.nrg.xdat.bean.XnatCtscandataFocalspotBean;
import org.nrg.xdat.bean.base.BaseElement;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

import static org.nrg.dcm.DicomAttributes.*;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
final class CTScanAttributes {
    private CTScanAttributes() {} // no instantiation

    static public AttrDefs get() { return s; }

    static final private MutableAttrDefs s = new MutableAttrDefs(ImageScanAttributes.get());

    static {
        s.add(new VoxelResAttribute("parameters/voxelRes"));
        s.add(new OrientationAttribute("parameters/orientation"));
        s.add("parameters/rescale/intercept", RESCALE_INTERCEPT);
        s.add("parameters/rescale/slope", RESCALE_SLOPE);
        s.add("parameters/kvp", CT_KVP);
        s.add("parameters/acquisitionNumber", Tag.AcquisitionNumber);
        s.add("parameters/imageType", Tag.ImageType);
        s.add("parameters/options", Tag.ScanOptions);
        s.add("parameters/collectionDiameter", CT_DATA_COLLECTION_DIAMETER);
        s.add("parameters/distanceSourceToDetector", CT_DISTANCE_SOURCE_TO_DETECTOR);
        s.add("parameters/distanceSourceToPatient", Tag.DistanceSourceToPatient);
        s.add("parameters/gantryTilt", CT_GANTRY_DETECTOR_TILT);
        s.add("parameters/tableHeight", CT_TABLE_HEIGHT);
        // Neurologica CereTom produces noncompliant DICOM. Thanks, Neurologica.
        s.add(TransformingExtAttrDef.wrap(new XnatAttrDef.Text("parameters/rotationDirection", CT_ROTATION_DIRECTION), "CCW", "CC"));
        s.add(new ExposureTimeAttribute("parameters/exposureTime", "CT", Tag.CTExposureSequence));
        s.add(new XRayTubeCurrentAttribute("parameters/xrayTubeCurrent", "CT", Tag.CTExposureSequence));
        s.add(new ExposureAttribute("parameters/exposure", "CT", Tag.CTExposureSequence));
        s.add("parameters/filter", CT_FILTER_TYPE);
        s.add("parameters/generatorPower", Tag.GeneratorPower);
        s.add("parameters/focalSpots/focalSpot", CT_FOCAL_SPOTS);	// requires additional processing from bean builder
        s.add("parameters/convolutionKernel", CT_CONVOLUTION_KERNEL);
        s.add("parameters/collimationWidth/single", CT_SINGLE_COLLIMATION_WIDTH);
        s.add("parameters/collimationWidth/total", CT_TOTAL_COLLIMATION_WIDTH);
        s.add("parameters/tableSpeed", CT_TABLE_SPEED);
        s.add("parameters/tableFeedPerRotation", CT_TABLE_FEED_PER_ROTATION);
        s.add("parameters/pitchFactor", CT_SPIRAL_PITCH_FACTOR);
        s.add(new XnatAttrDef.ValueWithAttributes("parameters/estimatedDoseSaving", CT_ESTIMATED_DOSE_SAVING,
                "modulation", CT_EXPOSURE_MODULATION_TYPE));
        s.add("parameters/ctDIvol", CT_DIVOL);
        s.add("parameters/derivation", Tag.DerivationDescription);
        s.add(new ImageFOVAttribute("parameters/fov"));
    }

    private final static Set<String> nullValues = ImmutableSet.of("", "null");

    private final static String FOCAL_SPOTS = "parameters/focalSpots/focalSpot";
    private final static BeanBuilder focalSpotsBeanBuilder = new BeanBuilder() {
        private final static String FOCAL_SPOT_SEPARATOR = "\\\\";
        public Collection<BaseElement> buildBeans(final ExtAttrValue focalSpotsValue)
        throws ConversionFailureException {
            final Collection<BaseElement> beans = Lists.newArrayList();
            final String focalSpots = focalSpotsValue.getText();
            if (null == focalSpots || nullValues.contains(focalSpots)) {
                return beans;
            }
            for (final String focalSpot : focalSpots.split(FOCAL_SPOT_SEPARATOR)) {
                final XnatCtscandataFocalspotBean focalSpotBean = new XnatCtscandataFocalspotBean();
                focalSpotBean.setFocalspot(focalSpot);
                beans.add(focalSpotBean);
            }
            return beans;
        }
    };

    private final static Map<String,BeanBuilder> beanBuilders = ImmutableMap.of(FOCAL_SPOTS, focalSpotsBeanBuilder);

    public final static Map<String,BeanBuilder> getBeanBuilders() { return beanBuilders; }
}
