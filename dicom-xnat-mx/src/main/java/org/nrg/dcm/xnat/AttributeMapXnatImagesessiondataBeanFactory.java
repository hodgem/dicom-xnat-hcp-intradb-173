/*
 * dicom-xnat-mx: org.nrg.dcm.xnat.AttributeMapXnatImagesessiondataBeanFactory
 * XNAT http://www.xnat.org
 * Copyright (c) 2017, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.dcm.xnat;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.SetMultimap;
import org.nrg.attr.ConversionFailureException;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.DicomMetadataStore;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.slf4j.Logger;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import static org.nrg.dcm.Attributes.StudyInstanceUID;

/**
 * @author Kevin A. Archie &lt;karchie@wustl.edu&gt;
 *
 */
public class AttributeMapXnatImagesessiondataBeanFactory implements XnatImagesessiondataBeanFactory {
    private final Logger logger;
    private final DicomAttributeIndex attribute;
    private final Map<String,Class<? extends XnatImagesessiondataBean>> classes;
    private final Function<Set<String>,String> leadExtractor;
    
    public AttributeMapXnatImagesessiondataBeanFactory(final DicomAttributeIndex attribute,
            final Map<String,String> keysToDataTypes,
            final Function<Set<String>,String> leadExtractor,
            final Logger logger) {
        this.attribute = attribute;
        this.leadExtractor = leadExtractor;
        this.logger = logger;
        
        this.classes = ImmutableMap.copyOf(Maps.transformValues(keysToDataTypes, XnatClassMapping.forBaseClass(XnatImagesessiondataBean.class)));
    }

    private static <K,C> C getInstance(final K key, final Map<? extends K,Class<? extends C>> classes) {
        if (null == key) {
            return null;
        }
        final Class<? extends C> clazz = classes.get(key);
        if (null == clazz) {
            return null;
        }
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.dcm.xnat.XnatImagesessiondataBeanFactory#create(org.nrg.dcm.DicomMetadataStore, java.lang.String)
     */
    @Override
    public XnatImagesessiondataBean create(DicomMetadataStore store, String studyInstanceUID) {
        return create(store, studyInstanceUID, null);
    }

    @Override
    public XnatImagesessiondataBean create(final DicomMetadataStore store, final String studyInstanceUID, final Map<String, String> parameters) {
        // Next try simple SOP class mapping
        final Map<DicomAttributeIndex,ConversionFailureException> failures = Maps.newLinkedHashMap();
        SetMultimap<DicomAttributeIndex, String> values;
        try {
            values = store.getUniqueValuesGiven(ImmutableMap.of(StudyInstanceUID,studyInstanceUID),
                    Collections.singleton(attribute), failures);
        } catch (Throwable t) {
            logger.error("unable to convert attribute " + attribute, t);
            return null;
        }
        
        for (final Map.Entry<DicomAttributeIndex,ConversionFailureException> fme : failures.entrySet()) {
            logger.error("unable to convert " + fme.getKey(), fme.getValue());
        }
        
        if (null == values) {
            return null;
        }
        final Set<String> v = values.get(attribute);
        if (null == v || v.isEmpty()) {
            return null;
        }
        return getInstance(leadExtractor.apply(v), classes);
    }
}
